import json

DATAFILE = 'config.json'

class NVStorage:
    
    def __init__(self):
        self._read()
        
    def __getitem__(self, key):
        return self.content[key]

    def save(self):
        with open(DATAFILE, "w") as f:
            json.dump(self.content, f)

    def _read(self):
        with open(DATAFILE, "r") as f:
            self.content = json.load(f)

if __name__ == "__main__":
    manager = NVStorage()
    print(manager["wifi"])
    print(manager["time"])
    print(manager["leds"])
