import neopixel
import asyncio
import machine

LEDS_PER_SIDE = 38
NLEDS = LEDS_PER_SIDE * 3
SIDE_LED_INDEXES = list(range(0, LEDS_PER_SIDE))
TOP_LED_INDEXES = list(range(SIDE_LED_INDEXES[-1]+1, LEDS_PER_SIDE*2))
SIDE_LED_INDEXES.extend(range(TOP_LED_INDEXES[-1]+1, NLEDS))


def get_led_color(led, top_colour, bottom_color):
    if led in TOP_LED_INDEXES:
        r,g,b = top_colour
    elif led < LEDS_PER_SIDE:
        distance_from_top = (LEDS_PER_SIDE-1) - led
        distance_from_bottom = led
        r = ((top_colour[0] *  distance_from_bottom) + (bottom_color[0] * distance_from_top)) / (LEDS_PER_SIDE-1)
        g = ((top_colour[1] *  distance_from_bottom) + (bottom_color[1] * distance_from_top)) / (LEDS_PER_SIDE-1)
        b = ((top_colour[2] *  distance_from_bottom) + (bottom_color[2] * distance_from_top)) / (LEDS_PER_SIDE-1)
    else:
        distance_from_top = led - (LEDS_PER_SIDE*2)
        distance_from_bottom = NLEDS - led
        r = ((top_colour[0] *  distance_from_bottom) + (bottom_color[0] * distance_from_top)) / (LEDS_PER_SIDE-1)
        g = ((top_colour[1] *  distance_from_bottom) + (bottom_color[1] * distance_from_top)) / (LEDS_PER_SIDE-1)
        b = ((top_colour[2] *  distance_from_bottom) + (bottom_color[2] * distance_from_top)) / (LEDS_PER_SIDE-1)

    # Note swap of G and B values in returned tuple!
    # This is intentional!
    return (int(r), int(b), int(g))


class LEDStrip:
    def __init__(self, nv_storage):
        self.pixels = neopixel.NeoPixel(machine.Pin(0), NLEDS)
        self.nv_storage = nv_storage
        self.top_colour = nv_storage["leds"]["top"]
        self.bottom_colour = nv_storage["leds"]["bottom"]
        print("LED: {},{},{} top, {},{},{} bottom".format(*self.top_colour, *self.bottom_colour))
        self.on = None
        self.update_evt = asyncio.Event()

    def set_top(self, top_colour):
        self.top_colour = top_colour
        self.nv_storage["leds"]["top"] = top_colour
        self.nv_storage.save()
        self.update_evt.set()

    def set_bottom(self, bottom_colour):
        self.bottom_colour = bottom_colour
        self.nv_storage["leds"]["bottom"] = bottom_colour
        self.nv_storage.save()
        self.update_evt.set()

    def enable(self, enable):
        if self.on != enable:
            self.on = enable
            self.update_evt.set()

    async def run(self):
        while True:
            await self.update_evt.wait()
            self.update_evt.clear()
            print("STRP: LEDs {}".format("on" if self.on else "off"))
            self.pixels.fill((0,0,0))
            if self.on:
                for i in range(NLEDS):
                    self.pixels[i] = get_led_color(i, self.top_colour, self.bottom_colour)
            self.pixels.write()
