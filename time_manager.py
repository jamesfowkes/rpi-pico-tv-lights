import machine
import time
import ntptime

BST_TIMESTAMPS = {
    2024: (1711846800, 1729994400),
    2025: (1743296400, 1761444000),
    2026: (1774746000, 1792893600),
    2027: (1806195600, 1824948000),
    2028: (1837645200, 1856397600),
    2029: (1869094800, 1887847200),
    2030: (1901149200, 1919296800),
    2031: (1932598800, 1950746400),
    2032: (1964048400, 1982800800),
    2033: (1995498000, 2014250400),
    2034: (2026947600, 2045700000),
    2035: (2058397200, 2077149600),
    2036: (2090451600, 2108599200),
    2037: (2121901200, 2140048800),
    2038: (2153350800, 2172103200),
    2039: (2184800400, 2203552800),
    2040: (2216250000, 2235002400)
}

class TimeManager:
    
    def __init__(self):
        self.rtc = machine.RTC()
        self.timer = machine.Timer()
        self.timer.init(period=86400, mode=machine.Timer.PERIODIC, callback=self.__ntp_update)

    def update(self):
        self.__ntp_update(None)
        self.timer.init(period=86400, mode=machine.Timer.PERIODIC, callback=self.__ntp_update)

    def __ntp_update(self, timer):
        # Set time from NTP (UTC time) and the UTC timestamp
        ntptime.settime()
        (year, month, mday, hour, minute, second, weekday, yearday) = time.localtime()
        timestamp = time.time()

        # Use UTC timestamp to decide if in BST
        bst_reference = BST_TIMESTAMPS[year]
        if (timestamp > bst_reference[0]) and (timestamp < bst_reference[1]):
            timestamp += 3600
        
        # Convert the local (GMT or BST) timestamp back into a tuple to pass to RTC
        # Note RTC requires a slightly different tuple than the time module returns
        (year, month, mday, hour, minute, second, weekday, yearday) = time.gmtime(timestamp)
        rtc_tuple = (year, month, mday, weekday, hour, minute, second, 0) # 0 is for subseconds
        self.rtc.datetime(rtc_tuple)

    def get(self):
        return self.rtc.datetime()
