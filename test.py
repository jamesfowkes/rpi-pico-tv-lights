# aiotest.py
# template asyncio application

import asyncio


class myapp:
    def __init__(self):
        pass

    async def coro1(self):
        while True:
            await asyncio.sleep_ms(1000)
            print("coro 1")

    async def coro2(self):
        while True:
            await asyncio.sleep_ms(2000)
            print("coro 2")

    async def coro3(self):
        while True:
            await asyncio.sleep_ms(3000)
            print("coro 3")

    async def blinker(self):
        self.led = machine.Pin("LED", machine.Pin.OUT)    # Pico W
        # self.led = machine.Pin(25, machine.Pin.OUT)    # Pico

        while True:
            self.led.value(1)
            await asyncio.sleep_ms(20)
            self.led.value(0)
            await asyncio.sleep_ms(980)

    async def run(self):
        print("app running - type ctrl-D to interrupt")

        t1 = asyncio.create_task(self.coro1())
        t2 = asyncio.create_task(self.coro2())
        t3 = asyncio.create_task(self.coro3())
        tf = asyncio.create_task(self.blinker())

        await asyncio.gather(t1, t2, t3, tf)

        print("app complete")


app = myapp()
asyncio.run(app.run())