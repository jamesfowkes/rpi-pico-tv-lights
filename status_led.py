import machine
import time

class StatusLED:
    
    def __init__(self):
        self.led = machine.Pin("LED", machine.Pin.OUT)
        self.timer = machine.Timer()
        self.set_freq(2)
        
    def blink(self, timer):
        self.led.toggle()

    def set_freq(self, freq):
        self.timer.init(freq=freq, mode=machine.Timer.PERIODIC, callback=self.blink)

if __name__ == "__main__":
    status_led = StatusLED()

    while True:
        pass
        