import machine
import time
import asyncio
import micropython

from status_led import StatusLED
from led_strip import LEDStrip
from wifi_manager import WiFiManager
from time_manager import TimeManager
from server import Server
from nv import NVStorage

def hhmmss_to_secs(hhmmss):
    try:
        hh, mm, ss = hhmmss
    except ValueError as e:
        hh, mm = hhmmss
        ss = 0

    return (hh * 3600) + (mm * 60) + ss

class LEDManager:
    
    def __init__(self, led_strip, nv_storage, time_provider):
        self.nv_storage = nv_storage
        self.on_time_secs = hhmmss_to_secs(nv_storage["time"]["on"])
        self.off_time_secs = hhmmss_to_secs(nv_storage["time"]["off"])
        print("TIM: {} on, {} off".format(self.on_time_secs, self.off_time_secs))
        self.test_mode = False
        self.time_provider = time_provider
        self.led_strip = led_strip
        
    def set_test_mode(self, test_mode):
        self.test_mode = test_mode
    
    def set_on_time(self, on_time):
        self.on_time_secs = hhmmss_to_secs(on_time)
        self.nv_storage["time"]["on"] = list(on_time)
        self.nv_storage.save()

    def set_off_time(self, off_time):
        self.off_time_secs = hhmmss_to_secs(off_time)
        self.nv_storage["time"]["off"] = list(off_time)
        self.nv_storage.save()

    def get_leds_on(self):
        if self.test_mode:
            return True
        else:
            (year, month, mday, hour, minute, second, weekday, yearday) = self.time_provider()
            seconds_now = hhmmss_to_secs((hour, minute, second))
            return self.on_time_secs < seconds_now < self.off_time_secs
    
    async def run(self):
        while True:
            self.led_strip.enable(self.get_leds_on())
            await asyncio.sleep_ms(1000)
            
if __name__ == "__main__":
    nv_storage = NVStorage()
    status_led = StatusLED()
    led_strip = LEDStrip(nv_storage)
    led_manager = LEDManager(led_strip, nv_storage, time.localtime)
    
    def handle_set_top_colour(url, data):
        # Assume data is a hex RGB triplet prepended with a URL-encoded '#' ('%23')
        data = data.replace("%23", "")
        r, g, b = int(data[0:2], 16), int(data[2:4], 16), int(data[4:6], 16)
        led_strip.set_top((r, g, b))

    def handle_set_bottom_colour(url, data):
        # Assume data is a hex RGB triplet prepended with a URL-encoded '#' ('%23')
        data = data.replace("%23", "")
        r, g, b = int(data[0:2], 16), int(data[2:4], 16), int(data[4:6], 16)
        led_strip.set_bottom((r, g, b))
    
    def handle_set_on_time(url, data):
        # Assume data is an hh:mm time with the ':' encoded as %3A
        data = data.replace("%3A", "")
        on_time_hhmm = (int(data[0:2]), int(data[2:4]))
        led_manager.set_on_time(on_time_hhmm)
        
    def handle_set_off_time(url, data):
        # Assume data is an hh:mm time with the ':' encoded as %3A
        data = data.replace("%3A", "")
        off_time_hhmm = (int(data[0:2]), int(data[2:4]))
        led_manager.set_off_time(off_time_hhmm)

    def handle_set_test_mode(url, data):
        led_manager.set_test_mode(data == "on")

    status_led.set_freq(5)
    wifi_manager = WiFiManager("Wifi Manager - TV Backlight", "picopico", nv_storage)
    time_manager = TimeManager()
    
    conn = wifi_manager.get_connection()
    if conn:
        status_led.set_freq(2)
        print("APP: Connected to WiFi at %s, trying NTP sync" % wifi_manager.ip())
        time_manager.update()
        print("Local datetime： %s" % str(time.localtime()))
        print("APP: Starting server")
        server = Server({
            "/top_rgb": handle_set_top_colour,
            "/bottom_rgb": handle_set_bottom_colour,
            "/on_time": handle_set_on_time,
            "/off_time": handle_set_off_time,
            "/test_mode": handle_set_test_mode
            },
            nv_storage,
            lambda: led_manager.test_mode
        )
    else:
        status_led.set_freq(10)

    async def main():        
        await asyncio.gather(server.run(), led_manager.run(), led_strip.run())

    asyncio.run(main())