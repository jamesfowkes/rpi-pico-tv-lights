import network
import socket
import ure
import time
import machine

from http import send_header, send_response
        
class WiFiManager:

    def __init__(self, ap_ssid, ap_password, nv_storage, secure_password_input=False):
        self.wlan_ap = network.WLAN(network.AP_IF)
        self.wlan_sta = network.WLAN(network.STA_IF)
        self.ap_ssid = ap_ssid
        self.ap_password = ap_password
        self.server_socket = None
        self.password_type = "password" if secure_password_input else "text"
        self.nv_storage = nv_storage

    def ip(self):
        return self.wlan_sta.ifconfig()[0]

    def reset(self):
        self.nv_storage["wifi"] = {}

    def get_connection(self):
        """return a working WLAN(STA_IF) instance or None"""

        for _ in range(3):
            if self.wlan_sta.isconnected():
                print("WFM: Already connected to " + self.wlan_sta.config('ssid'))
                self.wlan_ap.active(False)
                return self.wlan_sta
            time.sleep(1)
            
        connected = False
        try:
            # Search WiFis in range
            self.wlan_sta.active(True)
            networks = self.wlan_sta.scan()

            AUTHMODE = {0: "open", 1: "WEP", 2: "WPA-PSK", 3: "WPA2-PSK", 4: "WPA/WPA2-PSK"}
            for ssid, bssid, channel, rssi, authmode, hidden in sorted(networks, key=lambda x: x[3], reverse=True):
                ssid = ssid.decode('utf-8')
                encrypted = authmode > 0
                print("WFM: ssid: %s chan: %d rssi: %d authmode: %s" % (ssid, channel, rssi, AUTHMODE.get(authmode, '?')))
                if encrypted:
                    if ssid in self.nv_storage["wifi"]:
                        password = self.nv_storage["wifi"][ssid]
                        connected = self.do_connect(ssid, password)
                    else:
                        print("WFM: Skipping unknown encrypted network")
                else:  # open
                    pass
                if connected:
                    break

        except OSError as e:
            print("WFM: Exception", str(e))

        # start web server for connection manager:
        if not connected:
            connected = self.start()
        else:
            self.wlan_ap.active(False)

        return self.wlan_sta if connected else None


    def do_connect(self, ssid, password):
        self.wlan_sta.active(True)
        if self.wlan_sta.isconnected():
            return None
        print('WFM: Trying to connect to %s with password %s...' % (ssid, password))
        self.wlan_sta.connect(ssid, password)
        for retry in range(100):
            if self.wlan_sta.isconnected():
                break
            time.sleep(0.1)
            print('.', end='')

        if self.wlan_sta.isconnected():
            print('WFM: Connected. Network config: ', self.wlan_sta.ifconfig())
            self.wlan_ap.active(False)
        else:
            print('WFM: Failed. Not Connected to: ' + ssid)
        return self.wlan_sta.isconnected()

    def handle_root(self, client):
        self.wlan_sta.active(True)
        ssids = sorted(ssid.decode('utf-8') for ssid, *_ in self.wlan_sta.scan())
        send_header(client)
        client.sendall("""\
            <html>
                <h1 style="color: #5e9ca0; text-align: center;">
                    <span style="color: #ff0000;">
                        Wi-Fi Client Setup
                    </span>
                </h1>
                <form action="configure" method="post">
                    <table style="margin-left: auto; margin-right: auto;">
                        <tbody>
        """)
        while len(ssids):
            ssid = ssids.pop(0)
            client.sendall("""\
                            <tr>
                                <td colspan="2">
                                    <input type="radio" name="ssid" value="{0}" />{0}
                                </td>
                            </tr>
            """.format(ssid))
        client.sendall("""\
                            <tr>
                                <td>Password:</td>
                                <td><input name="password" type="{password_type}" /></td>
                            </tr>
                        </tbody>
                    </table>
                    <p style="text-align: center;">
                        <input type="submit" value="Submit" />
                    </p>
                </form>
                <p>&nbsp;</p>
                <hr />
                <hr />
            </html>
        """.format(password_type=self.password_type))
        client.close()

    def handle_configure(self, client, request):
        match = ure.search("ssid=([^&]*)&password=(.*)", request)

        if match is None:
            send_response(client, "Parameters not found", status_code=400)
            return False

        try:
            ssid = match.group(1).decode("utf-8").replace("%3F", "?").replace("%21", "!")
            password = match.group(2).decode("utf-8").replace("%3F", "?").replace("%21", "!")
        except Exception:
            ssid = match.group(1).replace("%3F", "?").replace("%21", "!")
            password = match.group(2).replace("%3F", "?").replace("%21", "!")

        ssid = ssid.replace("+", " ")

        if len(ssid) == 0:
            send_response(client, "SSID must be provided", status_code=400)
            return False

        if self.do_connect(ssid, password):
            response = """\
                <html>
                    <center>
                        <br><br>
                        <h1 style="color: #5e9ca0; text-align: center;">
                            <span style="color: #ff0000;">
                                Successfully connected to WiFi network %(ssid)s.
                            </span>
                        </h1>
                        <br><br>
                    </center>
                </html>
            """ % dict(ssid=ssid)
            send_response(client, response)
            self.nv_storage["wifi"][ssid] = password
            self.nv_storage.save()

            time.sleep(5)

            return True
        else:
            response = """\
                <html>
                    <center>
                        <h1 style="color: #5e9ca0; text-align: center;">
                            <span style="color: #ff0000;">
                                ESP could not connect to WiFi network %(ssid)s.
                            </span>
                        </h1>
                        <br><br>
                        <form>
                            <input type="button" value="Go back!" onclick="history.back()"></input>
                        </form>
                    </center>
                </html>
            """ % dict(ssid=ssid)
            send_response(client, response)
            return False

    def handle_not_found(self, client, url):
        send_response(client, "Path not found: {}".format(url), status_code=404)

    def stop(self):
        if self.server_socket:
            self.server_socket.close()
            self.server_socket = None

    def start(self, port=80):
        addr = socket.getaddrinfo('0.0.0.0', port)[0][-1]

        self.stop()

        self.wlan_sta.active(True)

        self.wlan_ap.config(essid=self.ap_ssid, password=self.ap_password)
        self.wlan_ap.active(True)

        self.server_socket = socket.socket()
        try:
            self.server_socket.bind(addr)
        except OSError:
            machine.reset()

        self.server_socket.listen(1)

        print('WFM: Connect to WiFi ssid ' + self.ap_ssid + ', default password: ' + self.ap_password)
        print('WFM: and access the Pico W at 192.168.4.1.')
        print('WFM: Listening on:', addr)

        while True:
            if self.wlan_sta.isconnected():
                return True

            client, addr = self.server_socket.accept()
            print('WFM: Client connected from', addr)
            try:
                client.settimeout(5.0)

                request = b""
                try:
                    while "\r\n\r\n" not in request:
                        request += client.recv(512)
                except OSError:
                    pass

                print("WFM: Request is: {}".format(request))
                if "HTTP" not in request:  # skip invalid requests
                    continue

                # version 1.9 compatibility
                try:
                    url = ure.search("(?:GET|POST) /(.*?)(?:\\?.*?)? HTTP", request).group(1).decode("utf-8").rstrip("/")
                except Exception:
                    url = ure.search("(?:GET|POST) /(.*?)(?:\\?.*?)? HTTP", request).group(1).rstrip("/")
                print("URL is {}".format(url))

                if url == "":
                    self.handle_root(client)
                elif url == "configure":
                    self.handle_configure(client, request)
                else:
                    self.handle_not_found(client, url)

            finally:
                client.close()
