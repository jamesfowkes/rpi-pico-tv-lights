import asyncio

TEMPLATE = """
<html>
    <head>
        <title>RPi Pico W TV Lights</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script>
            $(document).ready(function() {
                $('#topColourInput').on('input',
                    function(ev) {
                        $.ajax({
                              url: "/top_rgb",
                              type: 'PUT',
                              data: encodeURIComponent(ev.target.value),
                              success: $.noop
                        });
                    }
                );
                $('#bottomColourInput').on('input',
                    function(ev) {
                        $.ajax({
                              url: "/bottom_rgb",
                              type: 'PUT',
                              data: encodeURIComponent(ev.target.value),
                              success: $.noop
                        });
                    }
                );
                $('#onTimeInput').on('input',
                    function(ev) {
                        $.ajax({
                              url: "/on_time",
                              type: 'PUT',
                              data: encodeURIComponent(ev.target.value),
                              success: $.noop
                        });
                    }
                );
                $('#offTimeInput').on('input',
                    function(ev) {
                        $.ajax({
                              url: "/off_time",
                              type: 'PUT',
                              data: encodeURIComponent(ev.target.value),
                              success: $.noop
                        });
                    }
                );
                $('#testMode').on('input',
                    function(ev) {
                        $.ajax({
                              url: "/test_mode",
                              type: 'PUT',
                              data: ev.target.checked ? "on": "off",
                              success: $.noop
                        });
                    }
                );
            });
        </script>
    </head>
    <body>
        <div class="container">
        <h1>RPi Pico W TV Lights</h1>
        <form>
            <div class="container">
                <h2>Times</h2>
                <div class="form-group">
                    <label for="onTimeInput" class="form-label">On Time</label>
                    <input type="time" class="form-control" id="onTimeInput" value="%s" title="On Time">
                </div>
                <div class="form-group">
                    <label for="offTimeInput" class="form-label">Off Time</label>
                    <input type="time" class="form-control" id="offTimeInput" value="%s" title="Off Time">
                </div>              
            </div>
            <div class="container">    
                <h2>Colours</h2>
                <div class="form-group">
                    <label for="topColourInput" class="form-label">Top Colour</label>
                    <input type="color" class="form-control form-control-color" id="topColourInput" value="#%s" title="Choose top colour">
                </div>              
                <div class="form-group">
                    <label for="bottomColourInput" class="form-label">Bottom Colour</label>
                    <input type="color" class="form-control form-control-color" id="bottomColourInput" value="#%s" title="Choose bottom colour">
                </div>              
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="testMode" %s>
                    <label class="form-check-label" for="testMode">
                        Test Mode
                    </label>
                </div>
            </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
        </div>
    </body>
</html>
"""

class Server:
   
    def __init__(self, app_handlers, app_info, in_test_mode_fn):
        self.handlers = app_handlers
        self.app_info = app_info
        self.in_test_mode = in_test_mode_fn

    async def __serve_client(self, reader, writer):
        print("Client connected")
        request_line = await reader.readline()
        request = request_line.decode('utf-8').strip()
        print("Request:", request)
        
        if "PUT" in request:
            content_length = 0
            while True:
                header = await reader.readline()
                header = header.decode('utf-8').strip()
                if header.startswith("Content-Length: "):
                    content_length = int(header[16:])
                elif len(header) == 0:
                    break
            
            if content_length:
                request_data = await reader.readexactly(content_length)
                request_data = request_data.decode('utf-8')
            
            request = request[request.index("/"):] # Drop PUT
            request = request[:request.index(" HTTP")] # Drop HTTP suffix
        
            for handler_str, handler_fn in self.handlers.items():
                if request.startswith(handler_str):
                    print("Handler {} found for {}".format(handler_str, request))
                    handler_fn(request, request_data)
                    writer.write('HTTP/1.0 200 OK\r\n')

        elif "GET" in request:
            # Only GET request is for the site content, skip all headers
            while await reader.readline() != b"\r\n":
                pass
            writer.write('HTTP/1.0 200 OK\r\nContent-type: text/html\r\n\r\n')
            writer.write(TEMPLATE % (
                    "{}:{}".format(*self.app_info["time"]["on"]),
                    "{}:{}".format(*self.app_info["time"]["off"]),
                    "{:02X}{:02X}{:02X}".format(*self.app_info["leds"]["top"]),
                    "{:02X}{:02X}{:02X}".format(*self.app_info["leds"]["bottom"]),
                    "checked" if self.in_test_mode() else ""
                )
            )

        await writer.drain()
        await writer.wait_closed()
        print("Client disconnected")

    async def run(self):
        await asyncio.start_server(self.__serve_client, "0.0.0.0", 80)
